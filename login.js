const url = "https://sequlize-login.herokuapp.com/user/"

if (sessionStorage.getItem('token')) {
    const myHeaders = new Headers();
    myHeaders.append('x-auth-token', sessionStorage.getItem('token'))
    fetch(url, {
        method: 'get',
        headers: myHeaders
    }).then((res) => {
        return res.json()
    }).then(data=>{
        if(data.err){
            // removeItem()
        }
        else{
            window.location.assign('index.html')
            console.log("Success")
        }
    }).catch(Err => {
        console.log(Err)
    })
}

document.getElementById('form').addEventListener('submit', function (e) {

    e.preventDefault()
    const submitBtn = document.getElementById("submitBtn")
    submitBtn.disabled = true
    const formData = new FormData(document.getElementById('form'))
    const email = formData.get('email')
    const password = formData.get('password')
    fetch(url + 'login', {
        method: "POST",
        body: JSON.stringify({ email, password }),
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(response => {
            return response.json()
        })
        .then(data => {
            console.log(data)
            if (data.err) {
                document.getElementById('errMessage').innerText = data.message
            }
            else {
                sessionStorage.setItem('token', data.token)
                sessionStorage.setItem('email', email)
                sessionStorage.setItem('name', data.name)
                window.location.assign('index.html')
            }
            submitBtn.disabled = false
        }).catch(Err => {
            submitBtn.disabled = false
            console.log(Err)
        })

})

