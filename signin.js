const url = "https://sequlize-login.herokuapp.com/user/"

if (sessionStorage.getItem('token')) {
    const myHeaders = new Headers();
    myHeaders.append('x-auth-token', sessionStorage.getItem('token'))
    fetch(url, {
        method: 'get',
        headers: myHeaders
    }).then(data => {
        if (data.err) {
            // removeItem()
        }
        else {
            window.location.assign('index.html')
            console.log("Success")
        }
    }).catch(Err => {
        console.log(Err)
    })
}

document.getElementById('form').addEventListener('submit', function (e) {

    e.preventDefault()
    const submitBtn = document.getElementById("submitBtn")
    submitBtn.disabled = true
    const formData = new FormData(document.getElementById('form'))
    const email = formData.get('email')
    const password = formData.get('password')
    const fname = formData.get('fname')
    const lname = formData.get('lname')
    const confirm_password = formData.get('confirm_password')
    fetch(url + 'register', {
        method: "POST",
        body: JSON.stringify({ email, password, confirm_password, lname, fname }),
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(response => {
            return response.json()
        })
        .then(data => {
            console.log(data)
            if (data.err) {
                document.getElementById('errMessage').innerText = data.message
            }
            else {
                alert("signin successfull please login with the account!")
                window.location.assign('login.html')
            }
            submitBtn.disabled = false
        }).catch(Err => {
            submitBtn.disabled = false
            console.log(Err)
        })

})
