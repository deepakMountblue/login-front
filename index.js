if (!sessionStorage.getItem('token')) {
    location.assign('login.html')
}

const url = "https://sequlize-login.herokuapp.com/user/"

function removeSession() {
    sessionStorage.removeItem("token")
    sessionStorage.removeItem("name")
    sessionStorage.removeItem("email")
}


if (sessionStorage.getItem('token')) {
    const myHeaders = new Headers();
    myHeaders.append('x-auth-token', sessionStorage.getItem('token'))
    fetch(url + '/find', {
        method: 'get',
        headers: myHeaders
    }).then((res) => {
        return res.json()
    }).then(data => {
        if (data.err) {
            removeSession()
            window.location.assign('login.html')
        }
        else {
            const usersList = data.reduce((accumulator, cur) => {
                return(accumulator+`
                    <tr>
                        <td>${cur.id}</td>
                        <td>${cur.email}</td>
                        <td style="text-transform: capitalize;">${cur.fname} ${cur.lname}</td>
                    </tr>
                `)
            }, '')
            document.body.innerHTML = `
            <header>
            <div class="headerCont">
                 <h1>Sequelize Home</h1>
                 
                 <div class="user">
                     <h1 style="text-transform: capitalize;">${sessionStorage.getItem("name")}</h1>
                     <button onclick="logout()" class="logout">Logout</button>
                 </div>
            </div>
        </header>
        <div class="main" >
        <div class="details">
            <h1>Heyy!</h1>
            <h2>Here's the list of all the registerd users</h2>
            <table>
            <tr>
                <td>Id</td>
                <td>Email</td>
                <td>Name</td>
            </tr>
            ${usersList}
        </table>
        </div>
        <div class="Kidimage">
        <img src="./kid.jpg">
        </div>
    </div>
            `
        }
    })
        .catch(Err => {
            console.log(Err)
            // removeSession()
            // window.location.assign('login.html')
        })
}

function logout() {
    if (confirm("Are you sure you want to logout?")) {
        removeSession()
        window.location.assign('login.html')
    }
}

